from operator import add
from pyspark import SparkContext
from collections import Counter

def mapTags(line):
    firstTagEnd = line.index(">")
    tag = line[1:firstTagEnd]
    if firstTagEnd == len(line) - 1:
        return (tag, "")

    secondTagStart = line.index("</")

    return (tag, line[firstTagEnd + 1:secondTagStart])


def filterRelevant(elem):
    tag = elem[0]
    if tag == "title" or tag == "abstract":
        return True

    return False

def filterTitles(elem):
    tag = elem[0]
    return tag == "title"

def filterAbstracts(elem):
    tag = elem[0]
    return tag == "abstract"

# returns count elements with count >= num
def aboveNum(num):
    return lambda(countElem): countElem[1] > num

def belowIndex(ind):
    return lambda(indexedElem): indexedElem[1] < ind or ind == -1

gIndex = 0

def assignWithGlobalIndex(x):
    global gIndex
    indexedElem = (x, gIndex)
    gIndex += 1
    return indexedElem

# returns top num elements (as opposed to simply count >= num)
# do NOT want to apply an action, on purpose!
def topNum(countRdd, num):
    # Flip x,count to count,x
    # Filter all those with count == 1 - assumed to be the greater part of the RDD. Dataset assumed large enough.
    # Sort descending
    # Colesce to 1 partition - should be safe as we are left with a smaller dataset... hopefully
    postCoalescingCounts = countRdd.map(lambda x: (x[1], x[0])).filter(lambda x: x[0] != 1).sortByKey(False).coalesce(1)

    # If we have 1 partition it is safe to define a variable and use it locally in a single worker
    gIndex = 0
    topBeforeReorder = postCoalescingCounts.map(assignWithGlobalIndex).filter(belowIndex(num))
    print topBeforeReorder.collect()

    reorderedTopList = topBeforeReorder.map(lambda x: (x[0][1], x[0][0]))
    print reorderedTopList.collect()

    return reorderedTopList

meaninglessWords = set(["(disambiguation)","which","where","whose","whom",
                        "in","the","at","from","list","by" ,"st.","and" ,"of",
                        "wikipedia:","de","to","a","an","was","were","is","are","#redirect"])

def filterMeaninglessWords(w):
    wNormalized = w.lower()
    if  wNormalized in meaninglessWords:
        return False;

    return True;


def collectPopular(rdd, num):
    fullCount = rdd.flatMap(lambda s: s.split(" ")).filter(filterMeaninglessWords).map(lambda w: (w, 1)).reduceByKey(add)
    # return fullCount.filter(aboveNum(num))
    return topNum(fullCount, num)

# doing collectPopular on small lines, traditionally
def popularCalculation(x):
    tag = x[0]
    content = map(unicode.lower, x[1].split(" "))

    counters = Counter(w for w in content if w not in meaninglessWords)

    return counters


# TODO: Support num
def collectPopularPerTuple(rdd, num):
    fullCount = rdd.map(lambda x: (x[0], popularCalculation(x)))
    return fullCount

if __name__ == "__main__":
    sc = SparkContext(appName="WikipediaParsing")
    raw = sc.textFile("/home/shelly/Documents/Spark/enwiki-20151102-abstract25.xml")
    tagged = raw.map(mapTags)
    print tagged.take(30)
    filtered = tagged.filter(filterRelevant)
    print filtered.take(30)
    popularWordsInTitleWithCounts = collectPopular(filtered.filter(filterTitles).values(), 100)
    print popularWordsInTitleWithCounts.take(100)

    # just for fun
    # popularWordsInAbstractsWithCounts = collectPopular(filtered.filter(filterAbstracts).values(), -1)

    # get popular per title
    distributionOverTitle = collectPopularPerTuple(filtered.filter(filterAbstracts), -1)
    print distributionOverTitle.take(100)

    # attempt categorization by finding common word the highest counter in the abstract content word counter
    # very few will actually have any categories but this is completely OK and expected
