
from pyspark import SparkContext

if __name__ == "__main__":
    sc = SparkContext(appName="SieveOfPrimes")

    primeCandidates = sc.parallelize(range(100))
    composites = primeCandidates.cartesian(primeCandidates)
    compositesList = composites.flatMap(lambda (x,y): x*y if x!=1 and y!=1 else 1) # works only with map
    primes = primeCandidates.subtract(compositesList)
    primesSorted = sorted(primes.collect())
    print primesSorted
