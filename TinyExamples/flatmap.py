
from pyspark import SparkContext

g = 0

def mult(x, y):
    return {x*y*g}

if __name__ == "__main__":
    sc = SparkContext(appName="CartesianFlatMapMistake")
    simple = sc.parallelize(range(100))
    print simple.collect()
    cartesian = simple.cartesian(simple)
    # with mistake
    # print cartesian.flatMap(lambda(x,y): x*y).collect()
    # without mistake

    #print cartesian.flatMap(lambda(x,y): {x*y}).collect()

    print cartesian.flatMap(lambda(x,y): {x*y*g}).collect() # when g == 0, returns all 0s!

