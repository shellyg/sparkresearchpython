
def generatePics(succ, df, tr_txt, pngFileName):
    import os
    f = open("temp_chaotic.dt", "w")
    f.write("digraph cfg {\n")
    # write nodes and df values
    for node in succ:
        f.write("\t" + str(node) + " [label=\"df=" + str(df[node]) + "\"]\n")

    for u in succ:
        for v in succ[u]:
            f.write("\t" + str(u) + "->" + str(v) + " [label=\"" + tr_txt[(u,v)]+"\"]\n")
    f.write("\t}\n")
    f.close()
    os.system("dot temp_chaotic.dt -Tpng > " + pngFileName + "-spark.png")



def inWorklist(wl):
    return lambda x: x[0] in wl

def applyDfOnEdge(u, v, df, join, tr):
    return (v, join(tr[(u,v)](df[u]),df[v]))

def applyDf(df, join, tr):
    return lambda edge: applyDfOnEdge(edge[0], edge[1], df, join, tr)

def chaotic_spark(sc, succ, s, i, join, bottom, tr, tr_txt, pngFileName):
    # De-flat succ
    succDeflattened = []

    for node,successors in succ.iteritems():
        for succElem in successors:
            succDeflattened.append((node, succElem))

    print "Deflattened succ list: " + succDeflattened.__str__()

    wl = [s]
    df = dict([(x, bottom) for x in succ])
    df[s] = i

    succRdd = sc.parallelize(succDeflattened)
    print succRdd.collect()

    while wl != []:
        print "Current worklist: " + wl.__str__()
        print "Current df: " + df.__str__()

        currWorklistWithSuccRdd = succRdd.filter(inWorklist(wl))
        print currWorklistWithSuccRdd.collect()

        currDf = currWorklistWithSuccRdd.map(applyDf(df, join, tr))


        collectedCurrDf = currDf.collect()
        print collectedCurrDf

        appendToWl = []
        # Compare with previous df and merge into df.
        for elem in collectedCurrDf:
            u = elem[0]
            value = elem[1]
            if df[u] != value:
                print "changing the dataflow value at node: ", u, " to ", value
                df[u] = value
                appendToWl.append(u)

        # Remove from wl all keys in currWorklistWithSuccRdd, minus those that updated (appendToWl)
        for u in wl:
            if u not in appendToWl:
                wl.remove(u)

        for u in appendToWl:
            if u not in wl:
                wl.append(u)

        wl.sort(key=lambda x:-x) # sort in backward key order (required?)

    generatePics(succ, df, tr_txt, pngFileName)

# This is the naive implementation. Consider an implementation where each df value is a specialized accumulator