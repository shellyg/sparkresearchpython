import getopt
import sys
import imp
import os
from pyspark import SparkContext
from chaoticIteration import *

examples_path = "ChaoticIterations/src"
# first load all previous examples to path
sys.path.append(examples_path)

def print_usage():
    print "Chaotic Iterations Algorithm via Spark"
    print "\t-h - Print this help message"
    print "\t-f cfg file"

def get_filename(filename):
    pm = imp.load_source('module.name', examples_path + os.sep + filename)
    return pm

if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], "ha:f:", ["help", "abstraction=", "file="])
    except getopt.GetoptError:
        print "ERROR: bad usage"
        sys.exit(2)

    filename, abstractionFilename = [None, None]
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print_usage()
            sys.exit(0)
        elif opt in ("-a", "--abstraction"):
            abstractionFilename = arg
        elif opt in ("-f", "--file"):
            filename = arg

    if abstractionFilename is None:
        print "Please specify an abstraction file in " + examples_path
        print_usage()
        sys.exit(0)

    if filename is None:
        print "Please specify an example file in " + examples_path
        print_usage()
        sys.exit(0)

    print "loading and perhaps running the example:"
    abstractionModule = get_filename(abstractionFilename)
    module = get_filename(filename)
    print module

    print "running with Spark:"
    sc = SparkContext(appName="ChaoticIterationsNaive")
    chaotic_spark(sc, module.succ, 1, abstractionModule.iota, abstractionModule.join, abstractionModule.bottom, module.tr, module.tr_txt)


