bottom = set()
top = 'top' # artifricial top value to avoid the need to know how many variables are used via FrontEnd
iota=set()

def join(a, b):
    if a == top:
        return top
    elif b == top:
        return top
    else: return a | b

def meet(a, b):
    if a == top:
        return b
    elif b == top:
        return a
    else: return a & b
    
def removeLhs(lhs, pt):
     return set (p for p in pt if p[0] != lhs)

def set_addr(pt, lhs, a):
    # lhs = &a
    return removeLhs(lhs, pt) | set([(lhs, a)])

def copy_var(pt, lhs, rhs):
    # lhs = rhs
    return removeLhs(lhs, pt) | set((lhs, y) for (rhs, y) in pt)

def load(pt, lhs, rhs):
    # lhs = *rhs
    return removeLhs(lhs, pt) | set((lhs, z) for (y, z) in pt if (rhs, z) in pt)

def store(pt, lhs, rhs):
    # *lhs = rhs
    return pt | set([(y, z) for (l, y) in pt for (r, z) in pt if(l, r)==(lhs, rhs)])
