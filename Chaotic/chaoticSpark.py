
from pyspark import SparkContext
from chaoticIteration import *
from ChaoticIterations.src.chaotic import chaotic


class Abstraction(object):
    def join(a, b):
        pass

    def get_name(self):
        return self.__class__.__name


class AvailableExpressions(Abstraction):
    bottom = "bottom"  # artifricial bottomalue to avoid the need to know the set of used expressions
    top= set()
    iota=set()

    @staticmethod
    def join(a, b):
        if a == AvailableExpressions.bottom:
            return b
        elif b == AvailableExpressions.bottom:
            return a
        else: return a & b

    @staticmethod
    def meet(a, b):
        if a == AvailableExpressions.bottom:
            return AvailableExpressions.bottom
        elif b == AvailableExpressions.bottom:
            return AvailableExpressions.bottom
        else: return a | b

    @staticmethod
    def args(expr_tree):
         yield expr_tree[0]
         for subtree in expr_tree[1]:
                 for children in AvailableExpressions.args(subtree):
                         yield children

    @staticmethod
    def notArg(lhs, ae):
        if ae == AvailableExpressions.bottom: return AvailableExpressions.bottom
        return set (a for a in ae if lhs not in AvailableExpressions.args(a))

    @staticmethod
    def assign(ae, lhs, e):
        return AvailableExpressions.notArg(lhs, ae | set([e]))


class ConstantPropagation(Abstraction):
    bottom = "bottom"
    top = "top"
    iota = 0

    @staticmethod
    def join(a, b):
        if a == ConstantPropagation.bottom:
            return b
        elif b == ConstantPropagation.bottom:
            return a
        elif a == b:
            return a
        else: return ConstantPropagation.top

    @staticmethod
    def meet(a, b):
        if a == ConstantPropagation.top:
            return b
        elif b == ConstantPropagation.top:
            return a
        elif a == b:
            return a
        else: return ConstantPropagation.bottom


class PointsTo(Abstraction):
    bottom = set()
    top = 'top' # artifricial top value to avoid the need to know how many variables are used via FrontEnd
    iota=set()

    @staticmethod
    def join(a, b):
        if a == PointsTo.top:
            return PointsTo.top
        elif b == PointsTo.top:
            return PointsTo.top
        else: return a | b

    @staticmethod
    def meet(a, b):
        if a == PointsTo.top:
            return b
        elif b == PointsTo.top:
            return a
        else: return a & b

    @staticmethod
    def removeLhs(lhs, pt):
         return set (p for p in pt if p[0] != lhs)

    @staticmethod
    def set_addr(pt, lhs, a):
        # lhs = &a
        return PointsTo.removeLhs(lhs, pt) | set([(lhs, a)])

    @staticmethod
    def copy_var(pt, lhs, rhs):
        # lhs = rhs
        return PointsTo.removeLhs(lhs, pt) | set((lhs, y) for (rhs, y) in pt)

    @staticmethod
    def load(pt, lhs, rhs):
        # lhs = *rhs
        return PointsTo.removeLhs(lhs, pt) | set((lhs, z) for (y, z) in pt if (rhs, z) in pt)

    @staticmethod
    def store(pt, lhs, rhs):
        # *lhs = rhs
        return pt | set([(y, z) for (l, y) in pt for (r, z) in pt if(l, r)==(lhs, rhs)])


class Example(object):
    def __init__(self, succ, tr, tr_txt, pngFileName):
        self.succ = succ
        self.tr = tr
        self.tr_txt = tr_txt
        self.pngFileName = pngFileName


def run_chaotic_spark_on_abstraction(sc, abstraction, example):

    chaotic_spark(sc,
                  example.succ,
                  1, # assumed to start with 1, Always
                  abstraction.iota,
                  abstraction.join,
                  abstraction.bottom,
                  example.tr,
                  example.tr_txt,
                  example.pngFileName)

runCentralizedChaotic = True

def run_chaotic_centralized_on_abstraction(sc, abstraction, example):
    if runCentralizedChaotic:
        chaotic(example.succ,
                1, # assumed to start with 1, Always
                abstraction.iota,
                abstraction.join,
                abstraction.bottom,
                example.tr,
                example.tr_txt,
                example.pngFileName)

    else:
        print "Centralized Chaotic Iterations running is disabled"

def checkAvailableExpressions():
    availableExpressions = AvailableExpressions()

    ae_prog1 = Example({1:{2}, 2:{2,3}, 3:{}},
                       {(1,2): lambda ae: AvailableExpressions.assign(ae, 'x', ('+', (('y',()), ('2',())))), \
                        (2, 2): lambda x: x, \
                        (2, 3): lambda x: x},
                       {(1,2): "x := y + 2", (2, 2): "nop", (2, 3): "nop"},
                       "ae_prog1")

    ae_prog2 = Example({1:{2}, 2:{2,3}, 3:{}},
                        {(1,2): lambda ae: AvailableExpressions.assign(ae, 'x', ('+', (('y',()), ('2',())))), \
                            (2, 2): lambda x: x,\
                            (2, 3): lambda ae: AvailableExpressions.assign(ae, 'y',('+', (('y',()), ('1',()))))\
                        },
                        {(1,2): "x := y + 2", (2, 2): "nop", (2, 3): "x := x +1"},
                        "ae_prog2")

    run_chaotic_centralized_on_abstraction(sc, availableExpressions, ae_prog1)
    run_chaotic_spark_on_abstraction(sc, availableExpressions, ae_prog1)

    run_chaotic_centralized_on_abstraction(sc, availableExpressions, ae_prog2)
    run_chaotic_spark_on_abstraction(sc, availableExpressions, ae_prog2)

def checkConstantPropagation():
    constantPropagation = ConstantPropagation()

    cp_prog1 = Example({1:{2}, 2:{2,3}, 3:{}},
                       {(1,2): lambda x: 3, (2, 2): lambda x: x, (2, 3): lambda x: x},
                       {(1,2): "x := 3", (2, 2): "nop", (2, 3): "nop"},
                       "cp_prog1")

    cp_prog2 = Example({1:{2}, 2:{2,3}, 3:{}},
                        {(1,2): lambda x: 3, (2, 2): lambda x: 4, (2, 3): lambda x: x},
                        {(1,2): "x := 3", (2, 2): "x := 4", (2, 3): "nop"},
                       'cp_prog2')

    run_chaotic_centralized_on_abstraction(sc, constantPropagation, cp_prog1)
    run_chaotic_spark_on_abstraction(sc, constantPropagation, cp_prog1)

    run_chaotic_centralized_on_abstraction(sc, constantPropagation, cp_prog2)
    run_chaotic_spark_on_abstraction(sc, constantPropagation, cp_prog2)

def checkPointsTo():
    pointsTo = PointsTo()

    pt_prog1 = Example({1:{2}, 2:{2,3}, 3:{}},
                        {(1,2): lambda pt: PointsTo().set_addr(pt, 'x', 'a'), \
                        (2, 2): lambda x: x,\
                        (2, 3): lambda x: x},
                        {(1,2): "x := &a", (2, 2): "nop", (2, 3): "nop"},
                       "pt_prog1")

    pt_prog2 = Example({1:{2}, 2:{3}, 3:{4}, 4:{5, 6}, 5: {7}, 6:{7}, 7: {8}, 8: {}},
                        {(1,2): lambda pt: PointsTo.set_addr(pt, 't', 'a'), \
                          (2,3): lambda pt: PointsTo.set_addr(pt, 'y', 'b'), \
                          (3,4): lambda pt: PointsTo.set_addr(pt, 'z', 'c'), \
                          (4, 5): lambda pt: pt,\
                          (4, 6): lambda pt: pt,\
                          (5, 7): lambda pt: PointsTo.set_addr(pt, 'p', 'y'), \
                          (6, 7): lambda pt: PointsTo.set_addr(pt, 'p', 'z'), \
                         (7, 8): lambda pt: PointsTo.store(pt, 'p', 't')},
                       {(1,2): "t := &a", \
                       (2,3): "y := &b", \
                       (3,4): "z := &c", \
                       (4,5): "assume x >0", \
                       (4,6): "assume x <=0", \
                       (5, 7): "p := &y", \
                       (6, 7): "p := &z", \
                       (7, 8): "*p := t"},
                       "pt_prog2")


    run_chaotic_centralized_on_abstraction(sc, pointsTo, pt_prog1)
    run_chaotic_spark_on_abstraction(sc, pointsTo, pt_prog1)

    run_chaotic_centralized_on_abstraction(sc, pointsTo, pt_prog2)
    run_chaotic_spark_on_abstraction(sc, pointsTo, pt_prog2)

def finish():
    while(True):
        import time
        time.sleep(100)

if __name__ == "__main__":
    sc = SparkContext(appName="ChaoticIterationsNaive")

    # These basic abstractions and examples proved to be correct - (almost) equal pictures in the end
    # checkAvailableExpressions() # completely equal
    # checkConstantPropagation() # completely equal

    # equal up to ordering of the set in pt_prog2. Nevertheless correct
    checkPointsTo()

    # Go ahead with new examples and abstract domains!



    # This is an inifinte loop because we want to monitor the Spark UI. Ctrl+C to exit
    print "Press Ctrl+C to exit"
    finish()