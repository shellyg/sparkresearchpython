import numpy as np
import collections
import types

class Spylearn:
  @staticmethod
  def histogram(rdd, range=None, bins=10):
    """
    Compute the histogram of an RDD.
    """
    def _bin(num, bin_edges):
      """
      Given a number and set of bins defined by edges, computes which bin the number
      lies in.  Lower edges are inclusive, higher are exclusive.
      """
      if num < bin_edges[0]:
        return []

      for i, edge in enumerate(bin_edges[1:]):
        if num < edge:
          return [i]

      return []

    if isinstance(bins, collections.Iterable):
      bin_edges = bins
    elif type(bins) is types.IntType:
      if range is None:
        raise TypeError("range argument required when bins is an int")
      print "Creating bin edges from %s to %s with %d bins" % (range[0], range[1], bins)
      bin_edges = np.linspace(range[0], range[1], bins+1)
    else:
      raise TypeError("bins required to be an int or iterable")

    flatMapped = rdd.flatMap(lambda x: _bin(x, bin_edges))
    histogram = (flatMapped.countByValue(), bin_edges)
    print rdd.collect()
    print flatMapped.collect()
    print histogram
    print bin_edges
    return histogram