import getopt
import sys
import imp
import os
from pyspark import SparkContext

from histogramSpylearn import Spylearn
from histogramSynthesis import Synthesized

sc = SparkContext(appName="HistogramRunner")

rdd = sc.parallelize([1,2,3,4,2,3,2,24,45,2,3,42,342,3,42,43,24,52,11,341,12,41,4,3,9,6,5,46,45,70])

result1 = Spylearn.histogram(rdd, [0,100], 3)
result2 = Synthesized.histogram(rdd, [0,100], 3)

print result1
print result2


